use 5.006;
use strict;
use warnings;
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME                => 'Geo::Melissa::WebSmart',
    AUTHOR              => q{Mark Wells <mark@freeside.biz>},
    VERSION_FROM        => 'WebSmart.pm',
    ABSTRACT_FROM       => 'WebSmart.pm',
    ($ExtUtils::MakeMaker::VERSION >= 6.3002
      ? ('LICENSE'=> 'perl')
      : ()),
    PL_FILES            => {},
    PREREQ_PM => {
        'Test::More'     => 0,
        'XML::LibXML'    => 2,
        'LWP::UserAgent' => 0,
        'Crypt::SSLeay'  => 0,
    },
    dist                => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean               => { FILES => 'Geo-Melissa-WebSmart-*' },
);
