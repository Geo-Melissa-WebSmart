#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Geo::Melissa::WebSmart' ) || print "Bail out!\n";
}

diag( "Testing Geo::Melissa::WebSmart $Geo::Melissa::WebSmart::VERSION, Perl $], $^X" );
